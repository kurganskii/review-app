FROM node:12 as builder
WORKDIR /app
ENV CI true
COPY app/package.json app/yarn.lock ./
RUN yarn install
COPY app ./
RUN yarn build

FROM nginx:1.15.6-alpine as runner
WORKDIR /var/www
# Add nginx conf for react-router support
COPY nginx.conf /etc/nginx/nginx.conf
COPY --from=builder /app/build .
